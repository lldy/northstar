package tech.quantit.northstar.strategy.api;

import tech.quantit.northstar.common.TickDataAware;

/**
 * 组合指标接口
 * @author KevinHuangwl
 *
 */
public interface IComboIndicator extends BarDataAware, TickDataAware{

}
