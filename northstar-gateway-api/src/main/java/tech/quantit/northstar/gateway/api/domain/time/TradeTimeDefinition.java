package tech.quantit.northstar.gateway.api.domain.time;

import java.util.List;

public interface TradeTimeDefinition {

	List<PeriodSegment> getPeriodSegments();
}
